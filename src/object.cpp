#include "object.h"

#include <QMatrix4x4>

Object::Object(Model *model_ptr,
       QVector3D position, float scaling,
       float x_angle, float y_angle, float z_angle,
       qreal p_shining, qreal p_reflection_ratio)
    : model(model_ptr), shining(p_shining), reflection_ratio(p_reflection_ratio)
{
    matrix.rotate(QQuaternion::fromAxisAndAngle(QVector3D(0, 1, 0), y_angle));
    matrix.rotate(QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), x_angle));
    matrix.rotate(QQuaternion::fromAxisAndAngle(QVector3D(0, 0, 1), z_angle));
    matrix.scale(scaling);
    ray_dir_matrix = matrix.inverted();

    matrix.setToIdentity();

    matrix.translate(position);
    matrix.rotate(QQuaternion::fromAxisAndAngle(QVector3D(0, 1, 0), y_angle));
    matrix.rotate(QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), x_angle));
    matrix.rotate(QQuaternion::fromAxisAndAngle(QVector3D(0, 0, 1), z_angle));
    matrix.scale(scaling);
    ray_pos_matrix = matrix.inverted();

    for (const auto &triangle : model->triangles) {
        for (int i = 0; i < 3; ++i) {
            box.addPoint(matrix * triangle.points[i].position);
        }
    }
    box.finalize();
}

QPair<const Triangle *, QVector3D> Object::intersect(const Ray &ray) const {
    Ray local_ray(ray_pos_matrix * ray.start, ray_dir_matrix * ray.direction, ray.brightness);
    qreal best_dist = 1e+20;
    QVector3D best_position;
    const Triangle *best_triangle = nullptr;
    for (const auto &t : model->triangles) {
        qreal dist = 0;
        QVector3D position;
        if (t.intersect(local_ray, dist, position) && dist < best_dist) {
            best_triangle = &t;
            best_dist = dist;
            best_position = position;
        }
    }
    return qMakePair(best_triangle, matrix * best_position);
}

bool Object::intersectBoundingBox(const Ray &ray, qreal &distance) const {
    return box.intersect(ray, distance);
}

QVector3D Object::translate_point(QVector3D v) const
{
    return ray_pos_matrix * v;
}

QVector3D Object::translate_normal(QVector3D n) const
{
    return ray_dir_matrix.inverted() * n;
}
