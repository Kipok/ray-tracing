#include "triangle.h"

#include <QMatrix4x4>

bool Triangle::intersect(const Ray &ray, qreal &distance, QVector3D &position) const
{
    qreal r = QVector3D().distanceToPlane(points[1].position, points[0].position, points[2].position);
    QVector3D n = QVector3D::normal(sideVectors[0], -sideVectors[2]);
    qreal dn = QVector3D::dotProduct(ray.direction, n);
    if (fabs(dn) < 1e-7) {
        return false;
    }
    qreal an = QVector3D::dotProduct(ray.start, n);
    distance = (r - an) / dn;
    if (distance < 1e-5) {
        return false;
    }
    position = ray.start + distance * ray.direction;
    qreal square = QVector3D::crossProduct(sideVectors[0], sideVectors[1]).length();
    qreal sum_squares = 0;
    for (int i = 0; i < 3; ++i) {
        sum_squares += QVector3D::crossProduct(position - points[i].position, sideVectors[i]).length();
    }
    return square + 1e-4 > sum_squares;
}
