#include "ray.h"

#include "object.h"
#include "scene.h"

Ray::Ray(const QVector3D &s, const QVector3D &d, qreal b)
    : start(s), direction(d.normalized()), brightness(b) {}

QColor Ray::trace(const Scene &scene, int steps_left, qreal *depth) {
    if (steps_left == 0 || brightness < 0.001) {
        return Qt::black;
    }
    QVector<QPair<qreal, const Object *>> ordered_objects;
    for (const auto &obj : scene.objects) {
        qreal distance;
        if (obj.intersectBoundingBox(*this, distance) && fabs(distance) > 1e-5) {
            ordered_objects.push_back(qMakePair(distance, &obj));
        }
    }
    qSort(ordered_objects);
    QPair<const Triangle *, QVector3D> intersection;
    const Object *object = nullptr;
    bool intersection_found = false;
    for (const auto &obj : ordered_objects) {
        QPair<const Triangle *, QVector3D> cur_intersection = obj.second->intersect(*this);
        if (cur_intersection.first != nullptr) {
            intersection = cur_intersection;
            object = obj.second;
            intersection_found = true;
            break;
        }
    }
    if (!intersection_found) {
        return Qt::black;
    }
    QVector3D point = intersection.second;
    const Triangle &triangle = *intersection.first;

    if (depth != nullptr) {
        *depth = (point - start).length();
        return Qt::black;
    }

    QVector3D model_point = object->translate_point(point);
    qreal l0 = QVector3D::crossProduct(model_point - triangle.points[1].position, model_point - triangle.points[2].position).length();
    qreal l1 = QVector3D::crossProduct(model_point - triangle.points[0].position, model_point - triangle.points[2].position).length();
    qreal l2 = QVector3D::crossProduct(model_point - triangle.points[1].position, model_point - triangle.points[0].position).length();
    qreal ls = l0 + l1 + l2;
    l0 /= ls;
    l1 /= ls;
    l2 /= ls;
    QColor color = triangle.color;
    QVector3D normal = object->translate_normal((l0 * triangle.points[0].normal + l1 * triangle.points[1].normal +
                        l2 * triangle.points[2].normal)).normalized();
    QVector3D reflection = (direction - 2.0 * QVector3D::dotProduct(direction,normal) * normal).normalized();

    qreal diffuse_ratio = 0;

    for (const auto &light : scene.light_sources) {
        Ray shadow_ray(light.position, point - light.position, 1);
        qreal depth = 1e+20;
        shadow_ray.trace(scene, 1, &depth);
        if (fabs(depth - (point - light.position).length()) > 1e-2) {
            continue;
        }
        diffuse_ratio += std::max(0.0, QVector3D::dotProduct(normal, -shadow_ray.direction) * light.brightness);
    }
    diffuse_ratio = std::min(diffuse_ratio, 1.0);
    Ray next_ray(point, reflection, brightness);
    QColor next_color = next_ray.trace(scene, steps_left - 1);
    qreal diff_r, diff_g, diff_b, refl_r, refl_g, refl_b;
    color.getRgbF(&diff_r, &diff_g, &diff_b);
    next_color.getRgbF(&refl_r, &refl_g, &refl_b);

    qreal next_r = std::min(diff_r * diffuse_ratio + object->shining, 1.0) * (1 - object->reflection_ratio) +
                   object->reflection_ratio * refl_r;
    qreal next_g = std::min(diff_g * diffuse_ratio + object->shining, 1.0) * (1 - object->reflection_ratio) +
                   object->reflection_ratio * refl_g;
    qreal next_b = std::min(diff_b * diffuse_ratio + object->shining, 1.0) * (1 - object->reflection_ratio) +
                   object->reflection_ratio * refl_b;

    color.setRgbF(next_r, next_g, next_b);
    if (next_r < 1e-5) {
        next_r = next_r;
    }
    return color;
}
