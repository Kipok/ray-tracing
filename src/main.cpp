#include <QString>
#include <QImage>
#include <QFile>

#include "config.h"
#include "model.h"
#include "scene.h"
#include <ctime>
#include <iostream>
#include <omp.h>

int main(int argc, char *argv[])
{
    QString config_file = argc > 1 ? argv[1] : "default_config.json";
    const Config config(config_file);
    Scene world(config.scene);
    int time = clock();
    world.render(config);
    time = clock() - time;
    std::cout << "Time elapsed: " << time / (CLOCKS_PER_SEC * 60) << " minutes" << std::endl;
//    int tmp = 14123;
//    int time = clock();
//#pragma omp parallel for
//        for (int i = 1; i < 1000000000; i++) {
//            tmp /= i;
//            tmp *= i + 1;
//        }
//    time = clock() - time;
//    printf("%d %d\n", tmp, time / CLOCKS_PER_SEC);
}
