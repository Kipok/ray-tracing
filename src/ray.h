#pragma once

#include <QVector3D>
#include <QColor>
#include <QVector>

class Object;
class Scene;

class Ray
{
public:
    Ray(const QVector3D &s, const QVector3D &d, qreal b = 1);
    QColor trace(const Scene &scene, int steps_left = 2, qreal *depth = nullptr);

    QVector3D start, direction;
    qreal brightness;
};
