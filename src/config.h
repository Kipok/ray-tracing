#pragma once

#include <QString>
#include <QVector3D>
#include <QJsonArray>

struct Config
{
    Config(const QString &filename);
    static QVector3D getQVector(const QJsonArray &arr);
    int width, height, depth;
    QString scene, output;
    struct {
        QVector3D position, direction;
        qreal fov, focus;
    } camera;
};
