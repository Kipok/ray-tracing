#pragma once

#include "triangle.h"

#include <QVector>
#include <QString>

#include <QDebug>

class Model
{
public:
    Model(const QString &model_file);
    Model() {}
    void dumpRawData() {
        for (const auto &t : triangles) {
            qDebug() << t.points[0].position << t.points[0].normal
                     << t.points[1].position << t.points[1].normal
                     << t.points[2].position << t.points[2].normal
                     << t.color;
        }
    }

    QVector<Triangle> triangles;
};
