#-------------------------------------------------
#
# Project created by QtCreator 2014-12-10T19:16:47
#
#-------------------------------------------------

QT       += core gui

TARGET = RayTracer
CONFIG   += console
CONFIG   -= app_bundle
QMAKE_CXXFLAGS += -openmp -arch:AVX

TEMPLATE = app


SOURCES += main.cpp \
    config.cpp \
    scene.cpp \
    l3ds.cpp \
    boundingbox.cpp \
    model.cpp \
    object.cpp \
    ray.cpp \
    triangle.cpp

HEADERS += \
    config.h \
    scene.h \
    l3ds.h \
    triangle.h \
    boundingbox.h \
    model.h \
    object.h \
    ray.h

RESOURCES +=
