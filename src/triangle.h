#pragma once

#include "ray.h"

#include <QVector3D>
#include <QColor>

struct Triangle
{
    struct Point {
        Point() {}
        Point(const QVector3D &p, const QVector3D &n) :
            position(p), normal(n) {}

        QVector3D position;
        QVector3D normal;
    };

    Triangle() {}
    Triangle(const QVector3D &p0, const QVector3D &p1, const QVector3D &p2,
             const QVector3D &n0 = QVector3D(), const QVector3D &n1 = QVector3D(), const QVector3D &n2 = QVector3D(),
             const QColor &c = Qt::white) {
        points[0] = Point(p0, n0);
        points[1] = Point(p1, n1);
        points[2] = Point(p2, n2);
        color = c;
        for (int i = 0; i < 3; ++i) {
            sideVectors[i] = points[(i + 1) % 3].position - points[i].position;
        }
    }
    Triangle(const Point &p1, const Point &p2, const Point &p3, const QColor &c) {
        points[0] = p1;
        points[1] = p2;
        points[2] = p3;
        color = c;
    }

    bool intersect(const Ray &ray, qreal &distance, QVector3D &position) const;

    Point points[3];
    QColor color;
    QVector3D sideVectors[3];
};
