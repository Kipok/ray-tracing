#include "scene.h"

#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QImage>
#include <QJsonParseError>

void Scene::Grid::init(const Config &config) {
    cam_pos = config.camera.position;
    qreal grid_height = 2 * config.camera.focus * tan(config.camera.fov / 360 * M_PI);
    QVector3D grid_center = config.camera.position + config.camera.focus * config.camera.direction;
    qreal pixel_size = grid_height / config.height;
    x_step = QVector3D::normal(config.camera.direction, QVector3D(0, 1, 0)) * pixel_size;
    y_step = QVector3D::normal(config.camera.direction, x_step) * pixel_size;
    zero = grid_center - x_step * (config.width / 2.0) - y_step * (config.height / 2.0);
}

Ray Scene::Grid::getRay(int x, int y) {
    return Ray(cam_pos, zero + x * x_step + y * y_step - cam_pos);
}


Scene::Scene(const QString &filename) {
    QFile file(filename);
    file.open(QIODevice::ReadOnly);
    QJsonDocument json = QJsonDocument::fromJson(file.readAll());
    auto &json_models = json.object().find("models").value().toObject();
    for (auto it = json_models.begin(); it != json_models.end(); ++it) {
        models[it.key()] = Model(it.value().toString());
    }
    auto &json_objects = json.object().find("objects").value().toArray();
    for (auto &obj: json_objects) {
        objects.push_back(Object(
            &models[obj.toObject().find("model").value().toString()],
            Config::getQVector(obj.toObject().find("position").value().toArray()),
            obj.toObject().find("scaling").value().toDouble(),
            obj.toObject().find("x_angle").value().toDouble(),
            obj.toObject().find("y_angle").value().toDouble(),
            obj.toObject().find("z_angle").value().toDouble(),
            obj.toObject().find("shining").value().toDouble(),
            obj.toObject().find("reflection_ratio").value().toDouble()
        ));
    }
    auto &json_light_sources = json.object().find("light_sources").value().toArray();
    for (auto &ls: json_light_sources) {
        light_sources.push_back(LightSource(
            Config::getQVector(ls.toObject().find("position").value().toArray()),
            ls.toObject().find("brightness").value().toDouble()));
    }
}

void Scene::render(const Config &config) {
    grid.init(config);
    QImage result(config.width, config.height, QImage::Format_RGB888);
#pragma omp parallel for
    for (int y = 0; y < config.height; y++) {
        for (int x = 0; x < config.width; x++) {
            result.setPixel(x, y, grid.getRay(x, y).trace(*this, config.depth).rgb());
        }
    }
    result.save(config.output);
}
