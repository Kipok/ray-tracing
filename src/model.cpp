#include "model.h"

#include "l3ds.h"

#include <QVector3D>

template <typename T>
QVector3D toQVector3D(const T& vec) {
    return QVector3D(vec.x, vec.y, vec.z);
}

QColor toQColor(const LColor3 &c) {
    return QColor(c.r * 255, c.g * 255, c.b * 255);
}

Model::Model(const QString &model_file)
{
    L3DS reader(model_file.toLocal8Bit().constData());
    for (uint mesh_id = 0; mesh_id < reader.GetMeshCount(); ++mesh_id) {
        auto &mesh = reader.GetMesh(mesh_id);
        for (uint triangle_id = 0; triangle_id < mesh.GetTriangleCount(); ++triangle_id) {
            auto &triangle = mesh.GetTriangle2(triangle_id);
            triangles.push_back(Triangle(
                toQVector3D(triangle.vertices[0]),
                toQVector3D(triangle.vertices[1]),
                toQVector3D(triangle.vertices[2]),
                toQVector3D(triangle.vertexNormals[0]),
                toQVector3D(triangle.vertexNormals[1]),
                toQVector3D(triangle.vertexNormals[2]),
                toQColor(reader.GetMaterial(mesh_id).GetDiffuseColor())
            ));
            if (model_file == "data/models/rect.3ds") {
                triangles[triangles.size() - 1].color = QColor(200, 200, 200);
            }
        }
    }
}
