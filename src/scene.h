#pragma once

#define M_PI 3.1415926535897932384626433832795

#include <QMap>
#include <QString>
#include <QVector>
#include <QVector3D>

#include <cmath>

#include "config.h"
#include "ray.h"
#include "model.h"
#include "object.h"

class Scene
{
    struct Grid {
        QVector3D x_step, y_step, zero;
        QVector3D cam_pos;
        void init(const Config &config);
        Ray getRay(int y, int x);
    } grid;
    struct LightSource {
        LightSource(const QVector3D &p, qreal b) : position(p), brightness(b) {}
        LightSource() {}
        QVector3D position;
        qreal brightness;
    };
    QMap<QString, Model> models;
public:
    Scene(const QString &filename);
    void render(const Config &config);

    QVector<Object> objects;
    QVector<LightSource> light_sources;
};
