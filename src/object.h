#pragma once

#include "model.h"
#include "boundingbox.h"
#include "triangle.h"
#include "ray.h"

#include <QMatrix4x4>
#include <QVector3D>
#include <QString>
#include <QPair>

class Object
{
public:
    Object(Model *model,
           QVector3D position, float scaling,
           float x_angle, float y_angle, float z_angle, qreal shining, qreal reflection_ratio);
    Object() {}
    QPair<const Triangle *, QVector3D> intersect(const Ray &ray) const;
    bool intersectBoundingBox(const Ray &ray, qreal &distance) const;
    qreal shining, reflection_ratio;
    QVector3D translate_point(QVector3D) const;
    QVector3D translate_normal(QVector3D) const;
protected:
    Model *model;
    BoundingBox box;
    QMatrix4x4 matrix;
    QMatrix4x4 ray_pos_matrix;
    QMatrix4x4 ray_dir_matrix;
};
