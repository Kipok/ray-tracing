#include "boundingbox.h"

#include <algorithm>

using namespace std;

BoundingBox::BoundingBox() :
    min_coord( 1e+10,  1e+10,  1e+10),
    max_coord(-1e+10, -1e+10, -1e+10)
{}

void BoundingBox::addPoint(const QVector3D &point) {
    min_coord.setX(min(min_coord.x(), point.x()));
    min_coord.setY(min(min_coord.y(), point.y()));
    min_coord.setZ(min(min_coord.z(), point.z()));
    max_coord.setX(max(max_coord.x(), point.x()));
    max_coord.setY(max(max_coord.y(), point.y()));
    max_coord.setZ(max(max_coord.z(), point.z()));
}

QVector3D BoundingBox::getCoord(int code) {
    return QVector3D(
                (code & 1) == 0 ? min_coord.x() : max_coord.x(),
                (code & 2) == 0 ? min_coord.y() : max_coord.y(),
                (code & 4) == 0 ? min_coord.z() : max_coord.z());
}

void BoundingBox::addTriangles(int idx0, int idx1, int idx2, int idx3) {
    triangles.push_back(Triangle(getCoord(idx0), getCoord(idx1), getCoord(idx2)));
    triangles.push_back(Triangle(getCoord(idx1), getCoord(idx2), getCoord(idx3)));
}

void BoundingBox::finalize() {
    addTriangles(0, 1, 2, 3);
    addTriangles(4, 0, 6, 2);
    addTriangles(5, 4, 7, 6);
    addTriangles(1, 5, 3, 7);
    addTriangles(2, 3, 6, 7);
    addTriangles(4, 5, 0, 1);
}

bool BoundingBox::intersect(const Ray &ray, qreal &min_dist) const {
    min_dist = 1e+20;
    bool found = false;
    for (const auto &t : triangles) {
        qreal dist = 0;
        QVector3D position;
        if (t.intersect(ray, dist, position)) {
            min_dist = min(dist, min_dist);
            found = true;
        }
    }
    return found;
}
