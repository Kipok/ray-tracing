#pragma once

#include <QVector3D>
#include <QPair>
#include <QDebug>

#include "ray.h"
#include "triangle.h"

class BoundingBox
{
public:
    BoundingBox();
    void addPoint(const QVector3D &point);
    void finalize();
    bool intersect(const Ray &ray, qreal &min_dist) const;
    void dump_coords() const {
        qDebug() << min_coord << ' ' << max_coord << endl;
    }
protected:
    QVector3D min_coord, max_coord;
    QVector<Triangle> triangles;
    void addTriangles(int idx0, int idx1, int idx2, int idx3);
    QVector3D getCoord(int code);
};
