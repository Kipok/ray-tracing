#include "config.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QFile>

Config::Config(const QString &filename)
{
    QFile file(filename);
    file.open(QIODevice::ReadOnly);
    QJsonDocument json = QJsonDocument::fromJson(file.readAll());
    width  = json.object().find("width").value().toInt();
    height = json.object().find("height").value().toInt();
    depth  = json.object().find("depth").value().toInt();
    scene  = json.object().find("scene").value().toString();
    output = json.object().find("output").value().toString();
    QJsonObject &camera_json = json.object().find("camera").value().toObject();
    camera.position  = getQVector(camera_json.find("position").value().toArray());
    camera.direction = getQVector(camera_json.find("direction").value().toArray()).normalized();
    camera.fov   = camera_json.find("fov").value().toDouble();
    camera.focus = camera_json.find("focus").value().toDouble();
}

QVector3D Config::getQVector(const QJsonArray &arr) {
    return QVector3D(arr[0].toDouble(), arr[1].toDouble(), arr[2].toDouble());
}
